/**
 *
 * This file is part of the e2c3 project.
 * 
 * Copyright 2011, Ahmad Nazir Raja
 * 
 * The e2c3 source code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The e2c3 source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with the e2c3 project source code.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Apr 14, 2010
 *
 */

#ifndef EC_H_
#define EC_H_

/**
 * MACROS
 */
#define IPCOMP(addr, n) ((addr >> (24 - 8 * n)) & 0xFF)

/**
 * Global Declarations
 */
#define SIG_TEST 44	// Signal for kernel-user communication - debug purposes

#define EMULATE_WNIC 1 // account for WNIC transition times - sleep to wake and vice versa




#endif /* EC_H_ */
